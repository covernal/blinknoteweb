import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';

/*
  Generated class for the DBService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DBService {
	data = {}

	constructor(public http: Http, private storage: Storage) {
		console.log('Hello DBService Provider');
	}

	clearAndSave() {
		this.data = {};
		this.save();
	}

	read() {
		return new Promise((resolve, reject) => {
			this.storage.get('cache').then((value) => {
				if (value != undefined) {
					this.data = value;
					resolve(this);
				}
				else {
					this.data = {};
					resolve(this);
				}
			}).catch(() => {
				this.data = {};
				resolve(this);
			});
		});
	}
    save() {
        this.storage.set(`cache`, this.data);
    }

	token() { return this.data['token']; }
	setToken(token) { this.data['token'] = token; }

	profile() { return this.data['me']; }
	setProfile(me) { this.data['me'] = me }

}
