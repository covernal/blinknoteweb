import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the APIService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class APIService {
	_apihost = "http://ec2-52-55-219-203.compute-1.amazonaws.com:8080/api";
	token = "";

	constructor(public http: Http) {
		console.log('Hello APIService Provider');
	}

	apipath(path) { return this._apihost + path; }
	authHeader() {
		var headers = new Headers();
		headers.append('AUTH-TOKEN', this.token);
		return headers;
	}

	setToken(token) { this.token = token}

	login(email, password) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/login'), {
				'email': email, 'password': password
			}/*, { headers: headers }*/)
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});		
	}

	signup(data) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/signup'), data)
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	updateProfile(data) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/me'), data, { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	changePassword(oldPassword, newPassword) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/me/password'), {
				'oldPassword': oldPassword, 'newPassword': newPassword
			}, { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	searchPerson(searchKey) {
		return new Promise((resolve, reject) => {
			this.http.get(this.apipath('/persons/' + searchKey), { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	sendRequest(personID, message) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/contact/new/' + personID), {
				'message': message
			}, { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	pendingRequests() {
		return new Promise((resolve, reject) => {
			this.http.get(this.apipath('/contact/list/pending'), { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	deniedRequests() {
		return new Promise((resolve, reject) => {
			this.http.get(this.apipath('/contact/list/denied'), { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}	

	messagesInRoom(roomID) {
		return new Promise((resolve, reject) => {
			this.http.get(this.apipath('/chat/'+roomID+'/messages'), { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	contactList() {
		return new Promise((resolve, reject) => {
			this.http.get(this.apipath('/contact/list'), { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}

	sendMessage(roomID, message) {
		return new Promise((resolve, reject) => {
			this.http.post(this.apipath('/chat/'+roomID+"/message"), {
				'message': message
			}, { headers: this.authHeader() })
			.map(res => res.json())
			.subscribe(
				result => { resolve(result); },
				err => { reject(err); }
			);
		});
	}
}
