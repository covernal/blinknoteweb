import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FirebaseApp } from "angularfire2";
import * as firebase from "firebase";
import { AlertController } from 'ionic-angular';

/*
Generated class for the FcmService provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FcmService {
	private _messaging: firebase.messaging.Messaging;
	
	constructor(public http: Http, 
		public alertCtrl: AlertController,
		@Inject(FirebaseApp) private _firebaseApp: firebase.app.App) {
		console.log('Hello FcmService Provider');
        this._messaging = firebase.messaging(this._firebaseApp);
        
		// this._messaging.onTokenRefresh(this.initToken);

		this._messaging.onMessage(function(payload) {
			console.log("FCM - Message received. ", payload);
			payload.data.data = payload.data.data ? JSON.parse(payload.data.data) : undefined;
		});
	}

	private _askNotification(succssed, failed) {
        this._messaging.requestPermission()
        .then(() => {
			console.log('FCM - Notification permission granted.');
			succssed();
        })
        .catch((error) => {
        	console.log('FCM - Unable to get permission to notify.', error);
			let prompt = this.alertCtrl.create({
				title: 'BlinkNote',
				//message: "You have to allow notifications for BlinkNote, If not, you can't get new messages",
				message: "Notification is not allowed for BlinkNote,Please allow it to get new messages, and reload the page.",
				buttons: [
					// {
					// 	text: 'Cancel',
					// 	handler: data => {
					// 		console.log('Cancel clicked');
					// 		failed();
					// 	}
					// },
					{
						text: 'OK',
						handler: data => {
							failed();
							//this._askNotification(succssed, failed);
						}
					}
				]
			});
			prompt.present();        	
        });		
	}

	initToken() {
		return new Promise((resolve, reject) => {
			this._initToken(resolve, reject);
			// .then(function(currentToken) {
			// 	if (currentToken)
			// 		resolve(currentToken);
			// 	else {
			// 		// error -- No Instance ID token available. Request permission to generate one.
			// 		resolve(currentToken);
			// 	}
			// })
			// .catch(function(error) {
			// 	reject(error);
			// });
		});
	}

	private _initToken(resolve, reject) {
		//return new Promise((resolve, reject) => {
			let thiz = this;
			this._messaging.getToken()
			.then(function(currentToken) {
				if (currentToken) {
					resolve(currentToken);
				} else {
					console.log('FCM - No Instance ID token available. Request permission to generate one.');
					//resolve(currentToken);
					thiz._askNotification(function() {
						thiz._initToken(resolve, reject);
					}, function() {
						resolve(currentToken);
					})				
				}
			})
			.catch(function(err) {
				console.log('FCM - An error occurred while retrieving token. ', err);
				if(err['code'] == "messaging/notifications-blocked") {
					thiz._askNotification(function() {
						thiz._initToken(resolve, reject);
					}, function() {
						reject(err);
					});
				}
				reject(err);
			});
			// this._messaging.onTokenRefresh(function() {
			// 	this._messaging.getToken()
			// 	.then(function(refreshedToken) {
			// 		console.log('FCM - Token refreshed.');
			// 		resolve(refreshedToken);
			// 	})
			// 	.catch(function(err) {
			// 		console.log('FCM - Unable to retrieve refreshed token ', err);
			// 		reject(err);
			// 	});
			// });
		//});
	}

}
