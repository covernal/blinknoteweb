import { Component } from '@angular/core';

import { NavController, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { DashPage } from '../dash/dash';
import { PendingPage } from '../pending/pending';
import { SupportPage } from '../support/support';
import { ProfilePage } from '../profile/profile';

import { APIService } from '../../providers/api-service';
import { DBService } from '../../providers/db-service';

@Component({
	selector: 'page-declined',
	templateUrl: 'declined.html'
})
export class DeclinedPage {

constructor(public navCtrl: NavController,
		public apiService: APIService,
		public dbService: DBService,
		public loading: LoadingController,
		public toast: ToastController,
		public alertCtrl: AlertController) {}

	static cacheRooms = [];
	rooms = [];
	selected = undefined;
	me = [];
	
	ionViewDidLoad() {
		console.log('ionViewDidLoad DeclinedPage');
	}

	goDash() { this.navCtrl.setRoot(DashPage, {}); }
	
	goPending() { this.navCtrl.setRoot(PendingPage, {}); }

	goDeclined() { this.navCtrl.setRoot(DeclinedPage, {}); }
	
	goSupport() { this.navCtrl.setRoot(SupportPage, {}); }

	goProfile() { this.navCtrl.setRoot(ProfilePage, {}); }

	ionViewWillLoad() {
		this.me = this.dbService.profile();
		this.rooms = PendingPage.cacheRooms;

		let loader = this.loading.create({
			content: 'Loading...',
		});
		if(this.rooms.length == 0)
			loader.present();

		this.apiService.deniedRequests()
		.then(result => {
			console.log(result);
			if(this.rooms.length == 0) loader.dismiss();
			this.rooms = result['contacts'];
			PendingPage.cacheRooms = this.rooms;
		})
		.catch((ex) => {
			console.error('Error signup', ex);
			if(this.rooms.length == 0) loader.dismiss();
		});		
	}

	selectContact(room) {
		let _room = room;
		this.selected = room;
		if (!this.selected.messages) this.selected.messages = [];
		this.apiService.messagesInRoom(room.room_id)
		.then(result => {
			console.log(result);
			_room.messages = result['data'];
		})
		.catch((ex) => {
			console.error('Error signup', ex);
		});	
	}

	resend() {
		let prompt = this.alertCtrl.create({
			title: 'Resend your request',
			//message: "Send a request message again.",
			inputs: [
				{
					name: 'title',
					placeholder: 'Message here...',
					value: "Hi, I'd like contact with you."
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Send',
					handler: data => {
						console.log('Resend clicked', data);
						let personID = this.selected.user.id;
						let message = data['title'];
						let loader = this.loading.create({
							content: 'Sending request...',
						});
						loader.present()
						this.apiService.sendRequest(personID, message)
						.then(result => {
							console.log(result);
							loader.dismiss();
							this.selectContact(this.selected);
						})
						.catch((ex) => {
							console.error('Error signup', ex);
							loader.dismiss();
							this.toast.create({
									message: 'Failed sending.',
									duration: 3000,
									position: 'middle'
								}).present();
						});
					}
				}
			]
		});
		prompt.present();
	}
}
