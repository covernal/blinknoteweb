import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { DashPage } from '../dash/dash';
import { PendingPage } from '../pending/pending';
import { DeclinedPage } from '../declined/declined';
import { ProfilePage } from '../profile/profile';

@Component({
	selector: 'page-support',
	templateUrl: 'support.html'
})
export class SupportPage {

constructor(public navCtrl: NavController, public navParams: NavParams) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SupportPage');
	}

	goDash() { this.navCtrl.setRoot(DashPage, {}); }

	goPending() { this.navCtrl.setRoot(PendingPage, {}); }

	goDeclined() { this.navCtrl.setRoot(DeclinedPage, {}); }
	
	goSupport() { this.navCtrl.setRoot(SupportPage, {}); }

	goProfile() { this.navCtrl.setRoot(ProfilePage, {}); }
}
