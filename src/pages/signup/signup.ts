import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl} from '@angular/forms';
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";

import { NavController } from 'ionic-angular';
import { ForgotPage } from '../forgot/forgot';

import { APIService } from '../../providers/api-service';

@Component({
	selector: 'page-signup',
	templateUrl: 'signup.html',
	providers: [ APIService ]
})
export class SignupPage {

	result = {
		submitted : false,
		successed : false
	};

	model = {
		name: '',
		email: '',
		firstName: '',
		lastName: '',
		password: '',
		address: '',
		phone: '',
		description: ''
	}

	form: FormGroup;
	formErrors = {
		'name': [],
		'email': [],
		'firstName': [],
		'lastName': [],
		'password': [],
		'confirmPassword': [],
		'phone': [],
		'address': []
	};

	validationMessages = {
		'name': {
			'required':      'Organization name is required.',
			'minlength':     'Organization name must be at least 1 characters long.',
			'maxlength':     'Organization name cannot be more than 64 characters long.',
		},
		'email': {
			'required':      'Email is required.',
			'pattern':       'Enter a valid email.',
			'duplicated':	 'Duplicated email. Please input another email.'
		},
		'firstName': {
			'required':      'First name is required.'
		},
		'lastName': {
			'required':      'Last name is required.'
		},
		'phone': {
			'required':      'Phone is required',
			'pattern':       'Enter only numbers',
			'validatePhone': 'Phone incorrect for the country selected'
		},
		'password': {
			'required':      'Password is required',
			'minlength':     'Password must be at least 8 characters long.',
			'pattern':       'Your password must contain at least one uppercase, one lowercase, and one number.',
			'validateEqual': 'Password mismatch'
		},
		'confirmPassword':{
			'required':      'Confirm password is required',
			'minlength':     'Confirm password must be at least 8 characters long.',
			'pattern':       'Your password must contain at least one uppercase, one lowercase, and one number.',
			'validateEqual': 'Password mismatch'
		},
		'address': {
			'required':      'Address is required.'
		},
	}	

	constructor(public navCtrl: NavController, public apiService: APIService) {

	}

	ionViewWillLoad() {
		this.form = new FormGroup({
			name: new FormControl('', Validators.compose([
				Validators.minLength(1),
				Validators.maxLength(64),
				Validators.required
			])),
			email: new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
			firstName: new FormControl('', Validators.required),
			lastName: new FormControl('', Validators.required),
			phone: new FormControl('', Validators.compose([
				Validators.pattern('^\\d+$'),
				Validators.required
			])),
			address: new FormControl('', Validators.required),
			password: new FormControl('', Validators.compose([
				Validators.minLength(8),
				Validators.required,
				Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
			])),
			confirmPassword: new FormControl('', Validators.required),
			description: new FormControl('')
		});
		this.form.valueChanges
			.debounceTime(400.0)
			.subscribe(data => this.onValueChanged(data));		
	}

	onValueChanged(data?: any) {
		console.log(data);
		if (!this.form) { return; }
		const _form = this.form;
		for (const field in this.formErrors) {
			// clear previous error message
			this.formErrors[field] = [];
			this.form[field] = '';
			const control = _form.get(field);
			if (control /*&& control.dirty*/ && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field].push(messages[key]);
				}
			}
		}
		console.log(this.formErrors);
	}

	submit() {
		console.log("submitted", this.form.value, this.form.valid);
		this.form.value['type'] = 'organization';
		this.apiService.signup(this.form.value)
		.then(result => {
			console.log(result);
			this.result.submitted = true;
			this.result.successed = true;
		})
		.catch((ex) => {
			console.error('Error signup', ex);
			this.formErrors['email'] = [
				this.validationMessages['email']['duplicated']
			];
		});
	}

	cancel() {
		this.navCtrl.popToRoot();
	}

	forgot() {
		this.navCtrl.push(ForgotPage, {});
	}
}
