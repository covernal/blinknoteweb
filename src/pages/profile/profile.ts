import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl} from '@angular/forms';
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";

import { NavController, LoadingController, ToastController } from 'ionic-angular';

import { APIService } from '../../providers/api-service';
import { DBService } from '../../providers/db-service';

import { HomePage } from '../home/home';
import { DashPage } from '../dash/dash';
import { PendingPage } from '../pending/pending';
import { DeclinedPage } from '../declined/declined';
import { SupportPage } from '../support/support';

@Component({
	selector: 'page-profile',
	templateUrl: 'profile.html'
})
export class ProfilePage {
	select = "profile";

	me;
	updateForm: FormGroup;
	resetForm: FormGroup;
	formErrors = {
		'name': [],
		'email': [],
		'firstName': [],
		'lastName': [],
		'password': [],
		'confirmPassword': [],
		'phone': [],
		'address': [],
		'oldPassword': []
	};   

	validationMessages = {
		'name': {
			'required':      'Organization name is required.',
			'minlength':     'Organization name must be at least 1 characters long.',
			'maxlength':     'Organization name cannot be more than 64 characters long.',
		},
		'email': {
			'required':      'Email is required.',
			'pattern':       'Enter a valid email.',
			'duplicated':	 'Duplicated email. Please input another email.'
		},
		'firstName': {
			'required':      'First name is required.'
		},
		'lastName': {
			'required':      'Last name is required.'
		},
		'phone': {
			'required':      'Phone is required',
			'pattern':       'Enter only numbers',
			'validatePhone': 'Phone incorrect for the country selected'
		},
		'password': {
			'required':      'Password is required',
			'minlength':     'Password must be at least 8 characters long.',
			'pattern':       'Your password must contain at least one uppercase, one lowercase, and one number.',
			'validateEqual': 'Password mismatch'
		},
		'confirmPassword':{
			'required':      'Confirm password is required',
			'minlength':     'Confirm password must be at least 8 characters long.',
			'pattern':       'Your password must contain at least one uppercase, one lowercase, and one number.',
			'validateEqual': 'Password mismatch'
		},
		'address': {
			'required':      'Address is required.'
		},
	}		
	constructor(public navCtrl: NavController,
		public apiService: APIService,
		public dbService: DBService,
		public loading: LoadingController,
		public toast: ToastController) {
		this.me = dbService.profile();
	}

	goDash() { this.navCtrl.setRoot(DashPage, {}); }

	goPending() { this.navCtrl.setRoot(PendingPage, {}); }

	goDeclined() { this.navCtrl.setRoot(DeclinedPage, {}); }

	goSupport() { this.navCtrl.setRoot(SupportPage, {}); }

	logout() {
		this.apiService.setToken("");
		this.dbService.clearAndSave();
		this.navCtrl.setRoot(HomePage, {});
	}

	ionViewWillLoad() {
		console.log('ionViewDidLoad ProfilePage');

		this.updateForm = new FormGroup({
			name: new FormControl(this.me.name, Validators.compose([
				Validators.minLength(1),
				Validators.maxLength(64),
				Validators.required
			])),
			email: new FormControl(this.me.email),
			firstName: new FormControl(this.me.firstName, Validators.required),
			lastName: new FormControl(this.me.lastName, Validators.required),
			phone: new FormControl(this.me.phone, Validators.compose([
				Validators.pattern('^\\d+$'),
				Validators.required
			])),
			address: new FormControl(this.me.address, Validators.required),
			description: new FormControl(this.me.description)
		});
		this.updateForm.valueChanges
			.debounceTime(400.0)
			.subscribe(data => this.onValueChanged(data));
		this.resetForm = new FormGroup({
			oldPassword: new FormControl(""),
			password: new FormControl('', Validators.compose([
				Validators.minLength(8),
				Validators.required,
				Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
			])),
			confirmPassword: new FormControl('', Validators.required),
		});
		this.resetForm.valueChanges
			.debounceTime(400.0)
			.subscribe(data => this.onValue2Changed(data));		
	}

	onValueChanged(data?: any) {
		console.log(data);
		if (!this.updateForm) { return; }
		const _form = this.updateForm;
		for (const field in this.formErrors) {
			// clear previous error message
			this.formErrors[field] = [];
			this.updateForm[field] = '';
			const control = _form.get(field);
			if (control /*&& control.dirty*/ && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field].push(messages[key]);
				}
			}
		}
		console.log(this.formErrors);
	}
	onValue2Changed(data?: any) {
		console.log(data);
		if (!this.resetForm) { return; }
		const _form = this.resetForm;
		for (const field in this.formErrors) {
			// clear previous error message
			this.formErrors[field] = [];
			this.resetForm[field] = '';
			const control = _form.get(field);
			if (control /*&& control.dirty*/ && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field].push(messages[key]);
				}
			}
		}
		console.log(this.formErrors);
	}	

	update() {
		console.log("submitted", this.updateForm.value, this.updateForm.valid);
		let loader = this.loading.create({
			content: 'Updating...',
		});
		loader.present().then(() => {
			this.apiService.updateProfile(this.updateForm.value)
			.then(result => {
				console.log(result);
				this.me.name = this.updateForm.value.name;
				this.me.firstName = this.updateForm.value.firstName;
				this.me.lastName = this.updateForm.value.lastName;
				this.me.phone = this.updateForm.value.phone;
				this.me.address = this.updateForm.value.address;
				this.me.description = this.updateForm.value.description;

				this.dbService.setProfile(this.me);
				this.dbService.save();

				loader.dismiss();
				this.toast.create({
					message: 'Your profile has updated successfully',
					duration: 3000,
					position: 'middle'
				}).present();
			})
			.catch((ex) => {
				console.error('Error updateProfile', ex);
				loader.dismiss();
				this.toast.create({
					message: 'Failed update.',
					duration: 3000,
					position: 'middle'
				}).present();
			});
		});
	}

	reset() {
		console.log("submitted", this.resetForm.value, this.resetForm.valid);
		let loader = this.loading.create({
			content: 'Updating...',
		});
		loader.present().then(() => {		
			this.updateForm.value['type'] = 'organization';
			this.apiService.changePassword(this.resetForm.value['oldPassword'],
				this.resetForm.value['password'])
			.then(result => {
				console.log(result);
				loader.dismiss();
				this.toast.create({
					message: 'Your password has changed successfully',
					duration: 3000,
					position: 'middle'
				}).present();
			})
			.catch((ex) => {
				console.error('Error updateProfile', ex);
				loader.dismiss();
				this.formErrors.oldPassword = ["Wrong password, Please check again."]
				// this.toast.create({
				// 	message: 'You inputed wrong password. Please check again.',
				// 	duration: 3000,
				// 	position: 'middle'
				// }).present();
			});
		});
	}
}
