import { Component } from '@angular/core';

import { NavController, LoadingController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { DashPage } from '../dash/dash';

import { APIService } from '../../providers/api-service';
import { DBService } from '../../providers/db-service';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	data = {
		email: "org02@gmail.com",
		password: "qwerQWER1",
		needRemember: true
	};
	formErrors = [];

	constructor(public navCtrl: NavController,
		public apiService: APIService,
		public dbService: DBService,
		public loading: LoadingController) {
		
	}

	onSignup() {
		this.navCtrl.push(SignupPage, {});
	}

	onLogin() {
		let loader = this.loading.create({
			content: 'Logging in...',
		});
		loader.present().then(() => {
			this.apiService.login(this.data.email, this.data.password)
			.then(result => {
				console.log(result);
				if(this.data.needRemember) {
					this.apiService.setToken(result['token']);
					this.dbService.setToken(result['token']);
					this.dbService.setProfile(result['me']);
					this.dbService.save();
				}
				loader.dismiss();
				this.navCtrl.setRoot(DashPage, {});
			})
			.catch((ex) => {
				loader.dismiss();
				console.error('Error login', ex);
				if(ex.status = 401) {
					this.formErrors = ["Your email or password is not correct."];
				}
				if(ex.status = 403) {
					this.formErrors = ["Your account still not get activation. Please active your account."];
				}
			});
		});
	}
}
