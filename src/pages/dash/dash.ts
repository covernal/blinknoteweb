import { Component, ViewChild, ElementRef } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";

import { NavController, LoadingController, ToastController } from 'ionic-angular';

import { PendingPage } from '../pending/pending';
import { DeclinedPage } from '../declined/declined';
import { SupportPage } from '../support/support';
import { ProfilePage } from '../profile/profile';

import { APIService } from '../../providers/api-service';
import { DBService } from '../../providers/db-service';

@Component({
	selector: 'page-dash',
	templateUrl: 'dash.html'
})
export class DashPage {
	@ViewChild('chatArea') private chatAreaRef: ElementRef;

	searchKey = "";
	needSearch = true;
	isSearching = false;
	reqMessage = "";

	static cacheContacts = [];
	contacts = [];

	searchedPersons = [];

	me = undefined;
	view = {
		selected: undefined,
		selectedPerson: undefined
	};

	constructor(public navCtrl: NavController,
		public apiService: APIService,
		public dbService: DBService,
		public loading: LoadingController,
		public toast: ToastController) {		
	}

	goPending() { this.navCtrl.setRoot(PendingPage, {}); }

	goDeclined() { this.navCtrl.setRoot(DeclinedPage, {}); }
	
	goSupport() { this.navCtrl.setRoot(SupportPage, {}); }

	goProfile() { this.navCtrl.setRoot(ProfilePage, {}); }
	
	ionViewWillLoad() {
		this.me = this.dbService.profile();
		this.contacts = DashPage.cacheContacts;

		let loader = this.loading.create({
			content: 'Loading...',
		});
		if(this.contacts.length == 0)
			loader.present();

		this.apiService.contactList()
		.then(result => {
			console.log(result);
			if(this.contacts.length == 0) loader.dismiss();
			this.contacts = result['contacts'];
			DashPage.cacheContacts = this.contacts;
		})
		.catch((ex) => {
			console.error('Error signup', ex);
			if(this.contacts.length == 0) loader.dismiss();
		});
	}

	searchPerson() {
		if (!this.needSearch) return;
		if (this.searchKey == "") return;

		this.needSearch = false;
		this.isSearching = true;
		this.apiService.searchPerson(this.searchKey)
		.then(result => {
			console.log(result);
			this.isSearching = false;
			this.needSearch = false;

			this.searchedPersons = [];
			for (var _uk in result['data']) {
				let _u = result['data'][_uk];
				for (var _ck in this.contacts) {
					let _c = this.contacts[_ck];
					if (_c['user']['id'] == _u['id']) {
						_u = undefined; break;
					}
				}
				if(_u) this.searchedPersons.push(_u);
			}
		})
		.catch((ex) => {
			this.isSearching = false;
			this.needSearch = false;
			this.searchedPersons = [];
			console.error('Error signup', ex);
		});
	}

	selectContact(contact) {
		this.view.selected = contact;
		this.view.selectedPerson = undefined;
		console.log(this.view.selected);

		let _room = contact;
		if (!_room.messages) _room.messages = [];
		if (!_room.text) _room.text = "";

		this.apiService.messagesInRoom(_room.room_id)
		.then(result => {
			console.log(result);
			_room.messages = result['data'];
			_room.data = result;

			if(this.view.selected.room_id == _room.room_id) {
				setTimeout(()=>{ this.scrollToBottom(); }, 100);
				// set read flag for unread messages
			}
		})
		.catch((ex) => {
			console.error('Error signup', ex);
		});
	}

	sendMessage() {
		if (!this.view.selected) return;

		console.log(this.view.selected.text);
		let msgText = this.view.selected.text;

		if(msgText == "") {
			return;
		}

		let today = new Date();
		let _m = {
			created_at: today.toISOString().replace(/Z$/, ''),
			deleted_at: null,
			delivery_at: null,
			id: -Math.floor((Math.random() * 10000) + 1),
			message: msgText,
			room_id: this.view.selected.room_id,
			sender_id: this.me.id,
			updated_at: null
		};
		this.view.selected.messages = [_m].concat(this.view.selected.messages);
		this.view.selected.text = "";
		setTimeout(()=>{ this.scrollToBottom(); }, 100)

		this.apiService.sendMessage(this.view.selected.room_id, msgText)
		.then(result => {
			console.log(result);
			_m.id = result['message']['id'];
		})
		.catch((ex) => {
			console.error('Error signup', ex);
		});
	}

	selectPerson(person) {
		this.view.selectedPerson = person;
		this.view.selected = undefined;
		this.reqMessage = "Hi " + person.name + ", I'd like contact with you."
	}

	sendRequest() {
		console.log(this.reqMessage);
		let loader = this.loading.create({
			content: 'Sending request...',
		});
		loader.present()
		this.apiService.sendRequest(this.view.selectedPerson.id, this.reqMessage)
		.then(result => {
			console.log(result);
			this.isSearching = false;
			this.needSearch = false;

			this.searchedPersons = result['data'];
			loader.dismiss();
		})
		.catch((ex) => {
			console.error('Error signup', ex);

			this.isSearching = false;
			this.needSearch = false;
			this.searchedPersons = [];
			loader.dismiss();
			this.toast.create({
					message: 'Failed sending.',
					duration: 3000,
					position: 'middle'
				}).present();
		});
	}

	scrollToBottom() {
		try {
			this.chatAreaRef.nativeElement.scrollTop = this.chatAreaRef.nativeElement.scrollHeight;
		} catch(err) { }   
	}
}
