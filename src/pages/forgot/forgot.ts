import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl} from '@angular/forms';
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";

import { NavController } from 'ionic-angular';

@Component({
	selector: 'page-forgot',
	templateUrl: 'forgot.html',
})
export class ForgotPage {

	result = {
		submitted : false,
		successed : false
	};

	model = {
		name: '',
		email: '',
		firstName: '',
		lastName: '',
		password: '',
		address: '',
		phone: '',
		description: ''
	}

	form: FormGroup;
	formErrors = {
		'name': [],
		'email': [],
		'firstName': [],
		'lastName': [],
		'password': [],
		'confirmPassword': [],
		'phone': [],
		'address': []
	};

	validationMessages = {
		'email': {
			'required':      'Email is required.',
			'pattern':       'Enter a valid email.'
		},
		'phone': {
			'required':      'Phone is required',
			'pattern':       'Enter only numbers',
			'validatePhone': 'Phone incorrect for the country selected'
		}
	}

	constructor(public navCtrl: NavController) {

	}

	ionViewWillLoad() {
		this.form = new FormGroup({
			email: new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
			phone: new FormControl('', Validators.compose([
				Validators.pattern('^\\d+$'),
				Validators.required
			]))
		});
		this.form.valueChanges
			.debounceTime(400.0)
			.subscribe(data => this.onValueChanged(data));		
	}

	onValueChanged(data?: any) {
		console.log(data);
		if (!this.form) { return; }
		const _form = this.form;
		for (const field in this.formErrors) {
			// clear previous error message
			this.formErrors[field] = [];
			this.form[field] = '';
			const control = _form.get(field);
			if (control /*&& control.dirty*/ && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field].push(messages[key]);
				}
			}
		}
		console.log(this.formErrors);
	}

	submit() {
		console.log("submitted", this.form.value, this.form.valid);
	}

	cancel() {
		this.navCtrl.popToRoot();
	}
}
