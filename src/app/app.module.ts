import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MomentModule } from 'angular2-moment';
import { AngularFireModule } from 'angularfire2';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPage } from '../pages/forgot/forgot';
import { DashPage } from '../pages/dash/dash';
import { PendingPage } from '../pages/pending/pending';
import { DeclinedPage } from '../pages/declined/declined';
import { SupportPage } from '../pages/support/support';
import { ProfilePage } from '../pages/profile/profile';

import { SearchFilterPipe, ReversePipe } from '../pipes/search-filter';
import { EqualValidator } from '../validators';

import { APIService } from '../providers/api-service';
import { DBService } from '../providers/db-service';
import { FcmService } from '../providers/fcm-service';

export const firebaseConfig = {
  apiKey: "AIzaSyAA4xAfLeiJYebO_R7NR8ON7YUUzuS3yG4",
  authDomain: "blinknote-f0744.firebaseapp.com",
  databaseURL: "https://blinknote-f0744.firebaseio.com",
  storageBucket: "blinknote-f0744.appspot.com",
  messagingSenderId: "565961894289"
};

@NgModule({
  declarations: [    
    MyApp,
    EqualValidator, SearchFilterPipe, ReversePipe,
    HomePage, SignupPage, ForgotPage,
    DashPage, PendingPage, DeclinedPage, SupportPage, ProfilePage
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp, { 
      animate: false,
      //mode: 'md'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, SignupPage, ForgotPage,
    DashPage, PendingPage, DeclinedPage, SupportPage, ProfilePage
  ],
  providers: [Storage, APIService, DBService, FcmService, {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
