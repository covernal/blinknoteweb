import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { DashPage } from '../pages/dash/dash';

import { APIService } from '../providers/api-service';
import { DBService } from '../providers/db-service';
import { FcmService } from '../providers/fcm-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any

  constructor(platform: Platform,
    public apiService: APIService,
    public dbService: DBService,
    public fcmService: FcmService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      this.dbService.read()
      .then(dbs => {
        if (this.dbService.token() != undefined) {
          console.log("web token:" + this.dbService.token());
          this.apiService.setToken(this.dbService.token());
          this.rootPage = DashPage;
          this.fcmService.initToken()
          .then(token => {
            console.log("FCM Token: " + token)
            this.apiService.updateProfile({deviceToken: token})
            .then(result => {
              console.log("Device token updated on the server")
            })
            .catch(ex => {
            });
          })
          .catch(error => {
            console.log("FCM Token: " + "getting faield")
          });
        }
        else 
          this.rootPage = HomePage;   
      })
    });
  }
}
