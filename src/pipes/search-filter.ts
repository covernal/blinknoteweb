import { Injectable, Pipe, PipeTransform } from '@angular/core';

/*
  Generated class for the FilterPipe pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'searchFilter'
})
@Injectable()
export class SearchFilterPipe implements PipeTransform {
  transform(value, args) {
    return value.filter(contact => {
      console.log("searchKey:", args);
      if(args == undefined || args == "" ) 
        return true;
      return contact.user.name.toLowerCase().includes(args.toLowerCase());
    });
  }

  // transform(items: any[], field : string, value : string): any[] {  
  //     if (!items) return [];        
  //     return items.filter(it => it[field].includes(value));
  // }  
}

@Pipe({
  name: 'reverse'
})
@Injectable()
export class ReversePipe {
  transform(value) {
    return value.slice().reverse();
  }
}